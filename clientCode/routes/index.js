var express = require('express');
var router = express.Router();
const { clientApplication } = require('./client');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('manufacturer', { title: 'Manufacturer Dash' });
});

router.get('/table', function (req, res, next) {
  res.render('table', { title: 'table' });
});
router.get('/event', function (req, res, next) {
  res.render('event', { title: 'Event' });
});

router.post('/add', function (req, res) {
  const data = req.body;
  const carAuctionId = data.carAuctionId;
  const make = data.make;
  const model = data.model;
  const dom = data.dom;
  const owner = data.owner;
  const regNo = data.regNo;
  const chasisNo = data.chasisNo;

  let AuctionClient = new clientApplication();
  AuctionClient.setRoleAndIdentity('Auctioneer', 'admin');
  AuctionClient.initChannelAndChaincode('auction', 'CarAuctionContract');
  AuctionClient.generatedAndSubmitTxn(
    'createCarAuction',
    carAuctionId,
    make,
    model,
    dom,
    owner,
    regNo,
    chasisNo
  ).then((message) => {
    console.log(message);
  });
});

router.post('/read', async function (req, res) {
  const carAuctionId = req.body.carAuctionId;
  let AuctionClient = new clientApplication();
  AuctionClient.setRoleAndIdentity('Auctioneer', 'admin');
  AuctionClient.initChannelAndChaincode('auction', 'CarAuctionContract');
  AuctionClient.generatedAndSubmitTxn('readCarAuction', carAuctionId).then(
    (message) => {
      console.log(message.toString());
      res.send({ Cardata: message.toString() });
    }
  );
});

module.exports = router;
