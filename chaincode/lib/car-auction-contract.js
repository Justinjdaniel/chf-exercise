/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const { Contract } = require("fabric-contract-api");

class CarAuctionContract extends Contract {
    async carAuctionExists(ctx, carAuctionId) {
        const buffer = await ctx.stub.getState(carAuctionId);
        return !!buffer && buffer.length > 0;
    }

    async createCarAuction(
        ctx,
        carAuctionId,
        make,
        model,
        dom,
        owner,
        regNo,
        chasisNo
    ) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (exists) {
            throw new Error(`The car auction ${carAuctionId} already exists`);
        }
        const asset = {
            make,
            model,
            dom,
            owner,
            regNo,
            chasisNo,
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(carAuctionId, buffer);
    }

    async readCarAuction(ctx, carAuctionId) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        const buffer = await ctx.stub.getState(carAuctionId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateCarAuction(
        ctx,
        carAuctionId,
        make,
        model,
        dom,
        owner,
        regNo,
        chasisNo
    ) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        const asset = {
            make,
            model,
            dom,
            owner,
            regNo,
            chasisNo,
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(carAuctionId, buffer);
    }

    async deleteCarAuction(ctx, carAuctionId) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        await ctx.stub.deleteState(carAuctionId);
    }

    async openAuction(ctx, carAuctionId, faceValue) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        const carBuffer = await ctx.stub.getState(carAuctionId);
        const asset = JSON.parse(carBuffer.toString());
        asset = {
            price: faceValue,
            state: "open",
            status: "in auction",
            bidCount: "0",
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(carAuctionId, buffer);
    }

    async makeABid(ctx, carAuctionId, bidValue, name) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        const carBuffer = await ctx.stub.getState(carAuctionId);
        const asset = JSON.parse(carBuffer.toString());
        if (asset.state === "open") {
            const bidCount = asset.bidCount + 1;
            if (price < bidValue) {
                asset = { price: bidValue, bidCount, name };
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(carAuctionId, buffer);
            } else {
                throw new Error(
                    `The Bid Value for ${carAuctionId} is greater than your bid value`
                );
            }
        } else {
            throw new Error(`The acution for ${carAuctionId} is closed`);
        }
    }

    async closeAuction(ctx, carAuctionId) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        const carBuffer = await ctx.stub.getState(carAuctionId);
        const asset = JSON.parse(carBuffer.toString());
        asset = {
            state: "closed",
            status: "in review",
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(carAuctionId, buffer);
    }

    async winner(ctx, carAuctionId) {
        const exists = await this.carAuctionExists(ctx, carAuctionId);
        if (!exists) {
            throw new Error(`The car auction ${carAuctionId} does not exist`);
        }
        const carBuffer = await ctx.stub.getState(carAuctionId);
        const asset = JSON.parse(carBuffer.toString());
        if (asset.state === "closed") {
            const name = asset.name;
            const winner = `Assigned to ${name}`;
            asset = {
                state: winner,
                status: name,
            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(carAuctionId, buffer);
        }
    }
}

module.exports = CarAuctionContract;
